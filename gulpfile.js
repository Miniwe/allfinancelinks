const gulp = require('gulp');
const babel = require('gulp-babel');
const plugins = require('gulp-load-plugins')();
const browserSync = require('browser-sync').create();

gulp.task('clean', function() {
    return gulp.src('./dist', {read: false})
        .pipe(plugins.clean());
});

gulp.task('css', function() {
    return gulp.src([
        'node_modules/tether/src/css/tether.sass',
        'src/sass/main.scss'
    ])
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.sass().on('error', plugins.sass.logError))
        .pipe(plugins.cssmin())
        .pipe(plugins.autoprefixer())
        .pipe(plugins.sourcemaps.write())
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream());
});

gulp.task('js', () =>
    gulp.src([
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
        // 'src/base/scripts.js',
        // 'src/base/app.js',
        'src/js/templates.js',
        'src/js/index.js'
    ])
    // .pipe(babel({
    //     presets: ['env']
    // }))
    .pipe(plugins.sourcemaps.write())
    // .pipe(plugins.uglify())
    .pipe(plugins.concat('app.js'))
    .pipe(gulp.dest('dist/js'))
    .pipe(browserSync.stream())
);


gulp.task('watch', function(){
    gulp.watch('./src/sass/*.scss', ['css']);
    gulp.watch('./src/js/*.js', ['js']);
});

gulp.task('copy', function(){
    // return gulp.src(['./static/fonts/*'])
    //     // .pipe(gulpCopy('./static/fonts', {}))
    //     // .pipe(otherGulpFunction())
    //     .pipe(gulp.dest('./dist/css/static/fonts/'));

});

gulp.task('serve', function() {
    browserSync.init({
        server: {
            proxy: "http://localhost:3000/",
            baseDir: './'
        }
    });
    // gulp.watch(['*.html', 'templates/*.html']).on('change', browserSync.reload);
});


gulp.task('default', ['clean', 'css', 'js', 'copy', 'watch', 'serve']);