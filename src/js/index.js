$(document).ready(function () {
    // обработка мобильного меню

    $(".afl_navigation_menu_block-head").on("click", function () {
        if ($(window).width() < 768) {
            $("nav ul").hide();
            $(this).next("ul").toggle();
            $(this).next("ul").find("ul").toggle();
            return false;
        }
    });

    $('.navbar-toggler').on('click', function (event) {
        // console.log('clicked');
        $('.animated-icon', event.currentTarget).toggleClass('open');
        // $(this).toggleClass("open");
        if (!$('.animated-icon').hasClass("search")) {
            $(".afl_body .sidebar").toggleClass("in");
        } else {
            $(".search-btn").removeClass("active");
            $('.animated-icon', event.currentTarget).removeClass("search");
            $(".form-horizontal.afl_search").removeClass("active");
            $(".darked-bg").removeClass('active');
            setTimeout(function () {
                $(".darked-bg").hide();
            }, 500);
        }
    });

    $(".search-btn").click(function () {
        $(this).addClass("active");
        $(".navbar-toggler .animated-icon").addClass("open search");
        $(".afl_body .sidebar").removeClass("in");
        $(".form-horizontal.afl_search").addClass("active");
        $(".ya-site-form__input-text").focus();
        $(".darked-bg").show();
        setTimeout(function () {
            $(".darked-bg").addClass('active');
        }, 1);
    });

    $(".darked-bg").click(function () {
        $(".search-btn").removeClass("active");
        $(".navbar-toggler .animated-icon").removeClass("open search");
        $(".form-horizontal.afl_search").removeClass("active");
        $(".darked-bg").removeClass('active');
        setTimeout(function () {
            $(".darked-bg").hide();
        }, 500);
    });

    // кнопка наверх
    if (($(window).height() + 100) < $(document).height()) {
        $('#top-link-block').removeClass('hidden').affix({
            // how far to scroll down before link "slides" into view
            offset: {
                top: 100
            }
        });
    }

    // фикс для iPhone
    $(window).on("orientationchange", function () {
        window.location.reload();
    });

    $('.slider-links a').click(function () {
        $('.slider a:visible').fadeOut()
        $('#slider-' + $(this).data('slider')).fadeIn();
        return false;
    });

});
